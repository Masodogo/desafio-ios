//
//  BaseAPIClient.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Foundation
import Alamofire

class BaseAPIClient {
    
    static let sharedInstance = BaseAPIClient()
    private init() {}
    
    // MARK: - RESTFull Request
    
    func GET(url: String, headers: [String: String], completionHandler: Response<AnyObject, NSError> -> Void) {
        Alamofire.request(.GET, url, headers: headers, encoding: .JSON)
            .responseJSON { response in
                completionHandler(response)
        }
    }
    
    func POST(url: String, headers: [String: String], completionHandler: Response<AnyObject, NSError> -> Void) {
        Alamofire.request(.POST, url, headers: headers, encoding: .JSON)
            .responseJSON { response in
                completionHandler(response)
        }
    }
    
    func PATCH(url: String, headers: [String: String], completionHandler: Response<AnyObject, NSError> -> Void) {
        Alamofire.request(.PATCH, url, headers: headers, encoding: .JSON)
            .responseJSON { response in
                completionHandler(response)
        }
    }
    
    func PUT(url: String, headers: [String: String], completionHandler: Response<AnyObject, NSError> -> Void) {
        Alamofire.request(.PUT, url, headers: headers, encoding: .JSON)
            .responseJSON { response in
                completionHandler(response)
        }
    }
    
    func DELETE(url: String, headers: [String: String], completionHandler: Response<AnyObject, NSError> -> Void) {
        Alamofire.request(.DELETE, url, headers: headers, encoding: .JSON)
            .responseJSON { response in
                completionHandler(response)
        }
    }
}