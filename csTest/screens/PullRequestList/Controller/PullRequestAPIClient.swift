//
//  PullRequestAPIClient.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class PullRequestAPIClient: BaseAPIClient {
    
    static let baseAPIClient = BaseAPIClient.sharedInstance
    
    static func retrievePullRequestsWithRepository(repositoryName: String, owner: String, successBlock: (pullRequestDTO: Array<PullRequestDTO>) -> Void, failureBlock: Response<AnyObject, NSError> -> Void) {
        let path = "/repos/\(owner)/\(repositoryName)/pulls?state=all"
        baseAPIClient.GET("https://api.github.com" + path, headers: ["" : ""]) { (response: Response<AnyObject, NSError>) in
            switch(response.result){
            case .Success(let JSON):
                successBlock(pullRequestDTO: Mapper<PullRequestDTO>().mapArray(JSON)!)
                break
            case .Failure(_):
                failureBlock(response)
                break
            }
        }
    }
}
