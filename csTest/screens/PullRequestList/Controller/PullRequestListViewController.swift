//
//  PullRequestListViewController.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import UIKit
import Alamofire

class PullRequestListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var pullRequestTableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    internal var repositoryName = ""
    internal var username = ""
    private var pullRequestList: Array<PullRequestDTO> = Array<PullRequestDTO>()
    private var closedPullRequests: Int = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.repositoryName
        self.activityIndicator.startAnimating()
        PullRequestAPIClient.retrievePullRequestsWithRepository(self.repositoryName, owner: self.username, successBlock: { (pullRequestDTO) in
            self.activityIndicator.stopAnimating()
            self.pullRequestList = self.pullRequestListFiltered(pullRequestDTO)
            self.closedPullRequests = pullRequestDTO.count - self.pullRequestList.count
            self.pullRequestTableView.reloadData()
        }) { (error: Response<AnyObject, NSError>) in
            print(error)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func pullRequestListFiltered(pullRequestList: Array<PullRequestDTO>) -> Array<PullRequestDTO> {

        let filteredAttendees = pullRequestList.filter({
            $0.state == "open"
        })
        return filteredAttendees
    }
    
    //MARK - <UITableViewDataSource>
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(PullRequestTableViewCell.cellIdentifier(), forIndexPath: indexPath) as! PullRequestTableViewCell
        cell.configureCell(self.pullRequestList[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCellWithIdentifier(PullRequestHeaderCell.cellIdentifier()) as! PullRequestHeaderCell
        headerCell.configureCell(self.pullRequestList.count, closed: self.closedPullRequests)
                
        return headerCell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequestList.count
    }
    
    //MARK - <UITableViewDelegate>
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pullRequestDTO = self.pullRequestList[indexPath.row]
        UIApplication.sharedApplication().openURL(NSURL(string:pullRequestDTO.externalUrl)!)
    }
}