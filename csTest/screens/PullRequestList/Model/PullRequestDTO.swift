//
//  PullRequestDTO.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequestDTO: Mappable {
    
    var title: String = ""
    var fullname: String = ""
    var body: String = ""
    var user: OwnerDTO?
    var createdAt: NSDate?
    var externalUrl: String = ""
    var state: String = ""
    
    required init?(_ map: Map){
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        fullname <- map["full_name"]
        body <- map["body"]
        user <- map["user"]
        createdAt <- (map["created_at"], CustomDateFormatTransform(formatString: "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"))
        externalUrl <- map["html_url"]
        state <- map["state"]
    }
}