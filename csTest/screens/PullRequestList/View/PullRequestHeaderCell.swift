//
//  PullRequestHeaderCell.swift
//  csTest
//
//  Created by Diogo Autilio on 7/19/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import UIKit

class PullRequestHeaderCell: UITableViewCell {
    
    @IBOutlet var pullRequestsState: UILabel!
    
    internal static func cellIdentifier() -> String {
        return "PullRequestHeaderCell"
    }
    
    internal func configureCell(opened: Int, closed: Int) {
        let text = String(format: "%d opened / %d closed", opened, closed)
        var openedTextWithColor = text.componentsSeparatedByString("/")
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.orangeColor() , range: (text as NSString).rangeOfString(openedTextWithColor[0]))
        
        pullRequestsState.attributedText = attributedString
    }
    
    override func prepareForReuse() {
        pullRequestsState.text =  nil
    }
}