//
//  PullRequestTableViewCell.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import UIKit
import Kingfisher

class PullRequestTableViewCell: UITableViewCell {
    
    @IBOutlet var username: UILabel!
    @IBOutlet var userAvatar: UIImageView!
    @IBOutlet var pullRequestDescription: UILabel!
    @IBOutlet var pullRequestTitle: UILabel!
    @IBOutlet var createdAt: UILabel!
    
    internal static func cellIdentifier() -> String {
        return "PullRequestTableViewCell"
    }
    
    internal func configureCell(pullRequest: PullRequestDTO) {
        
        pullRequestDescription.text = pullRequest.body
        pullRequestTitle.text = pullRequest.title
        createdAt.text = pullRequest.createdAt?.timeAgoSinceDate()
        
        if let user = pullRequest.user {
            username.text = user.login
            
            userAvatar.layer.cornerRadius = userAvatar.frame.size.width / 2
            userAvatar.layer.masksToBounds = true
            userAvatar.layer.borderColor = UIColor.lightGrayColor().CGColor
            userAvatar.layer.borderWidth = 0.5
            
            userAvatar.kf_setImageWithURL(NSURL(string: user.avatarUrl)!, placeholderImage: UIImage.init(named: "github-default-user"))
        }
    }
    
    override func prepareForReuse() {
        pullRequestDescription.text = nil
        pullRequestTitle.text = nil
        createdAt.text = nil
        username.text = nil
        userAvatar.image = nil
    }
}