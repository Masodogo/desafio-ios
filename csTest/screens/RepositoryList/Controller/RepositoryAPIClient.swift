//
//  RepositoryAPIClient.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class RepositoryAPIClient: BaseAPIClient {
    
    static let baseAPIClient = BaseAPIClient.sharedInstance
    
    static func retrievePopularRepositories(page:Int, successBlock: (repositoryListDTO: RepositoryListDTO) -> Void, failureBlock: Response<AnyObject, NSError> -> Void) {
        let path = "/search/repositories?q=language:Java&sort=stars&page=\(page)"
        baseAPIClient.GET("https://api.github.com" + path, headers: ["" : ""]) { (response: Response<AnyObject, NSError>) in
            switch(response.result){
                case .Success(let JSON):
                    successBlock(repositoryListDTO: Mapper<RepositoryListDTO>().map(JSON)!)
                    break
                case .Failure(_):
                    failureBlock(response)
                    break
            }
        }
    }
}
