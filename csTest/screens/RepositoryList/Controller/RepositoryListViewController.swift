//
//  RepositoryListViewController.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import UIKit
import Alamofire

class RepositoryListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var repositoryTableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    private var currentPage = 1
    private var repositoryList = RepositoryListDTO()
    private var retrievingData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator.startAnimating();
        self.loadRepositoryWithPage(self.currentPage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadRepositoryWithPage(page: Int) {
        
        self.currentPage += 1
        retrievingData = true
        
        RepositoryAPIClient.retrievePopularRepositories(page, successBlock: { (repositoryListDTO) in
            self.retrievingData = false
            self.repositoryList.items.appendContentsOf(repositoryListDTO.items)
            self.activityIndicator.stopAnimating()
            self.repositoryTableView.reloadData()
        }) { (error: Response<AnyObject, NSError>) in
            print(error)
            self.retrievingData = false
            self.activityIndicator.stopAnimating()
        }
    }
    
    //MARK - <UIScrollViewDelegate>
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maxOffset - offset) <= 750 {
            if !self.retrievingData {
                self.loadRepositoryWithPage(self.currentPage)
            }
        }
    }

    //MARK - <UITableViewDataSource>
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(RepositoryTableViewCell.cellIdentifier(), forIndexPath: indexPath) as! RepositoryTableViewCell
        cell.configureCell(self.repositoryList.items[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositoryList.items.count
    }
    
    //MARK - <UITableViewDelegate>
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("PR_DETAILS_SEGUE", sender: self.repositoryList.items[indexPath.row])
    }
    
    //MARK - prepareForSegue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PR_DETAILS_SEGUE" {
            let repositoryDTO = sender as! RepositoryDTO
            let pullRequestListViewController = segue.destinationViewController as! PullRequestListViewController
            pullRequestListViewController.username = repositoryDTO.owner!.login
            pullRequestListViewController.repositoryName = repositoryDTO.name
        }
    }
}

