//
//  OwnerDTO.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Foundation
import ObjectMapper

class OwnerDTO: Mappable {
    
    var avatarUrl: String = ""
    var login: String = ""
    
    required init?(_ map: Map){
    }
    
    func mapping(map: Map) {
        avatarUrl <- map["avatar_url"]
        login  <- map["login"]
    }
}