//
//  RepositoryDTO.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Foundation
import ObjectMapper

class RepositoryDTO: Mappable {
    
    var name: String = ""
    var forksCount: NSNumber = 0
    var stargazersCount: NSNumber = 0
    var fullname: String = ""
    var description: String = ""
    var owner: OwnerDTO?
    
    required init?(_ map: Map){ 
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        forksCount  <- map["forks_count"]
        fullname <- map["full_name"]
        description <- map["description"]
        stargazersCount <- map["stargazers_count"]
        owner <- map["owner"]
    }
}