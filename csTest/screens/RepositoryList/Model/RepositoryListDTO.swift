//
//  RepositoryListDTO.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Foundation
import ObjectMapper

class RepositoryListDTO: NSObject, Mappable {
    
    var items: Array<RepositoryDTO> = Array<RepositoryDTO>()
    
    // Needed by RepositoryListViewController array initialization
    override init() {
        super.init()
    }
        
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        items <- map["items"]
    }
}
