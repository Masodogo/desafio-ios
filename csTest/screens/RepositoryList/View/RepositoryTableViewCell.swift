//
//  RepositoryTableViewCell.swift
//  csTest
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import UIKit
import Kingfisher

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet var repositoryName: UILabel!
    @IBOutlet var username: UILabel!
    @IBOutlet var repositoryDescription: UILabel!
    @IBOutlet var userAvatar: UIImageView!
    @IBOutlet var fullName: UILabel!
    @IBOutlet var gitBranchIcon: UIImageView!
    @IBOutlet var gitStarsIcon: UIImageView!
    @IBOutlet var gitStarsCount: UILabel!
    @IBOutlet var gitForksCount: UILabel!
    
    internal static func cellIdentifier() -> String {
        return "RepositoryTableViewCell"
    }
    
    internal func configureCell(repository: RepositoryDTO) {
        
        repositoryName.text = repository.name
        repositoryDescription.text = repository.description
        gitStarsCount.text = repository.stargazersCount.stringValue
        gitForksCount.text = repository.forksCount.stringValue
        fullName.text = repository.fullname

        if let owner = repository.owner {
            username.text = owner.login
            
            userAvatar.layer.cornerRadius = userAvatar.frame.size.width / 2
            userAvatar.layer.masksToBounds = true
            userAvatar.layer.borderColor = UIColor.lightGrayColor().CGColor
            userAvatar.layer.borderWidth = 0.5
            
            userAvatar.kf_setImageWithURL(NSURL(string: owner.avatarUrl)!, placeholderImage: UIImage.init(named: "github-default-user"))
        }
    }
    
    override func prepareForReuse() {
        repositoryName.text = nil
        repositoryDescription.text = nil
        gitStarsCount.text = nil
        gitForksCount.text = nil
        fullName.text = nil
        username.text = nil
        userAvatar.image = nil
    }
}