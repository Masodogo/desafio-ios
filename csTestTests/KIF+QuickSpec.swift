//
//  KIF+QuickSpec.swift
//  csTest
//
//  Created by Diogo Autilio on 7/19/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Quick
import Nimble
import KIF

extension QuickSpec {
    
    func tester(file : String = #file, _ line : Int = #line) -> KIFUITestActor {
        return KIFUITestActor(inFile: file, atLine: line, delegate: self)
    }
    
    func system(file : String = #file, _ line : Int = #line) -> KIFSystemTestActor {
        return KIFSystemTestActor(inFile: file, atLine: line, delegate: self)
    }
    
    override public func failWithException(exception: NSException!, stopTest stop: Bool) {
        if let userInfo = exception.userInfo {
            fail(exception.description,
                 file: userInfo["FilenameKey"] as! String,
                 line: userInfo["LineNumberKey"] as! UInt)
        } else {
            fail(exception.description)
        }
    }
}

