//
//  OwnerDTOTest.swift
//  csTest
//
//  Created by Diogo Autilio on 7/19/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Quick
import Nimble
import Alamofire
import ObjectMapper
@testable import csTest

class OwnerDTOTest: QuickSpec {
    override func spec() {
        describe("test OwnerDTO") {
            it("test Owner model with success") {
                let login = "johnDoe"
                let imageUrl = "https://www.super.url"
                
                let JSON = ["login": login, "avatar_url": imageUrl]
                let owner = Mapper<OwnerDTO>().map(JSON)
                
                expect(owner).toNot(beNil())
                expect(login).to(equal(owner!.login))
                expect(imageUrl).to(equal(owner!.avatarUrl))
            }
        }
    }
}

