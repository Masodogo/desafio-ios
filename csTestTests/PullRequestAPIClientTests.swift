//
//  PullRequestAPIClientTests.swift
//  csTest
//
//  Created by Diogo Autilio on 7/19/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Quick
import Nimble
import Alamofire
@testable import csTest

class PullRequestAPIClientTests: QuickSpec {
    override func spec() {
        describe("test PullRequestAPIClient") {
            it("test pull request populate with success") {
                waitUntil(timeout: 5.0) { completed in
                    PullRequestAPIClient.retrievePullRequestsWithRepository("Quick", owner: "Quick", successBlock: { (pullRequestDTO) in
                        expect(pullRequestDTO.isEmpty).to(beFalse())
                        completed()
                    }) { (error: Response<AnyObject, NSError>) in
                        print(error)
                    }
                }
            }
        }
    }
}
