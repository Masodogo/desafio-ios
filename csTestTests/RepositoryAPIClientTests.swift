//
//  RepositoryAPIClientTests.swift
//  csTest
//
//  Created by Diogo Autilio on 7/19/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Quick
import Nimble
import Alamofire
@testable import csTest

class RepositoryAPIClientTests: QuickSpec {
    override func spec() {
        describe("test RepositoryAPIClient") {
            it("test repository populate with success") {
                waitUntil(timeout: 5.0) { completed in
                    RepositoryAPIClient.retrievePopularRepositories(1, successBlock: { (repositoryListDTO) in
                        expect(repositoryListDTO.items.isEmpty).to(beFalse())
                        completed()
                    }) { (error: Response<AnyObject, NSError>) in
                        print(error)
                    }
                }
            }
        }
    }
}
