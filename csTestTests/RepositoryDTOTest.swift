//
//  RepositoryDTOTest.swift
//  csTest
//
//  Created by Diogo Autilio on 7/19/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Quick
import Nimble
import Alamofire
import ObjectMapper
@testable import csTest

class RepositoryDTOTest: QuickSpec {
    override func spec() {
        describe("test RepositoryDTO") {
            it("test RepositoryDTO model with success") {
                
                let ownerJSON = ["login": "facebook", "avatar_url": "https://avatars.githubusercontent.com/u/69631?v=3"]
                
                let name = "react-native"
                let fullname = "facebook/react-native"
                let forksCount = 7503
                let stargazersCount = 35073
                let description =  "A framework for building native apps with React."
                let owner = Mapper<OwnerDTO>().map(ownerJSON)
                
                let JSON = ["name": name, "full_name": fullname, "forks_count": forksCount, "stargazers_count": stargazersCount, "description": description, "owner": ownerJSON]
                let repository = Mapper<RepositoryDTO>().map(JSON)
                
                expect(repository).toNot(beNil())
                expect(name).to(equal(repository!.name))
                expect(fullname).to(equal(repository!.fullname))
                expect(forksCount).to(equal(repository!.forksCount))
                expect(stargazersCount).to(equal(repository!.stargazersCount))
                expect(description).to(equal(repository!.description))
                expect(owner?.login).to(equal(repository!.owner?.login))
                expect(owner?.avatarUrl).to(equal(repository!.owner?.avatarUrl))
            }
        }
    }
}
