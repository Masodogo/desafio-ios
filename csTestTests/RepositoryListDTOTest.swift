//
//  RepositoryListDTOTest.swift
//  csTest
//
//  Created by Diogo Autilio on 7/19/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Quick
import Nimble
import Alamofire
import ObjectMapper
@testable import csTest

class RepositoryListDTOTest: QuickSpec {
    override func spec() {
        describe("test RepositoryListDTO") {
            it("test RepositoryListDTO model with success") {
                
                let ownerJSON = ["login": "facebook", "avatar_url": "https://avatars.githubusercontent.com/u/69631?v=3"]
                
                let name = "react-native"
                let fullname = "facebook/react-native"
                let forksCount = 7503
                let stargazersCount = 35073
                let description =  "A framework for building native apps with React."
                let owner = Mapper<OwnerDTO>().map(ownerJSON)!
                
                let repositoryJSON = ["name": name, "full_name": fullname, "forks_count": forksCount, "stargazers_count": stargazersCount, "description": description, "owner": ownerJSON]
                
                let JSON: [String: AnyObject] = [ "items": [ repositoryJSON ] ]
                
                let repositoryList = Mapper<RepositoryListDTO>().map(JSON)
                
                expect(repositoryList).toNot(beNil())
                expect(repositoryList?.items.isEmpty).to(beFalse())

                expect(name).to(equal(repositoryList?.items.first?.name))
                expect(fullname).to(equal(repositoryList?.items.first?.fullname))
                expect(forksCount).to(equal(repositoryList?.items.first?.forksCount))
                expect(stargazersCount).to(equal(repositoryList?.items.first?.stargazersCount))
                expect(description).to(equal(repositoryList?.items.first?.description))
                expect(owner.login).to(equal(repositoryList?.items.first?.owner?.login))
                expect(owner.avatarUrl).to(equal(repositoryList?.items.first?.owner?.avatarUrl))
            }
        }
    }
}
