//
//  RepositoryListTestCase.swift
//  csTestTests
//
//  Created by Diogo Autilio on 7/18/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Quick
import Nimble

class RepositoryListTestCase: QuickSpec {
    override func spec() {
        describe("test repository screen") {
            it("test repository populate with success") {
                self.tester().waitForViewWithAccessibilityLabel("REPOSITORY_CELL")
            }
            it("test repository cell selection with success") {
                self.tester().tapViewWithAccessibilityLabel("REPOSITORY_CELL")
                self.tester().waitForViewWithAccessibilityLabel("PULL_REQUEST_SCREEN")
            }
            it("test pull request cell selection with success") {
                self.tester().tapViewWithAccessibilityLabel("REPOSITORY_CELL")
                self.tester().waitForViewWithAccessibilityLabel("PULL_REQUEST_SCREEN")
                self.tester().tapViewWithAccessibilityLabel("PULL_REQUEST_CELL")
            }
        }
    }
}