//
//  RepositoryTableViewCellTest.swift
//  csTest
//
//  Created by Diogo Autilio on 7/19/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import Quick
import Nimble
import Alamofire
import ObjectMapper
@testable import csTest

class RepositoryTableViewCellTest: QuickSpec {
    override func spec() {
        describe("test RepositoryTableViewCell") {
            it("should test RepositoryTableViewCell identifier and pass") {
                expect(RepositoryTableViewCell.cellIdentifier()).to(equal("RepositoryTableViewCell"))
            }
            it("should test RepositoryTableViewCell identifier and fail") {
                expect(RepositoryTableViewCell.cellIdentifier()).toNot(equal("NOPE"))
            }
        }
    }
}
